
int find( __global const ulong *, ulong target, ulong, ulong );
__kernel void check (
	__global char *ret,
	const ulong dim,
	__global const ulong *dims,
	__global const ulong *size,
	const ulong len,
	__global const ulong *pts
){
	const static ulong2 lim[] = {
		{ 5, 3 },
		{ 5, 3 },
		{ 3, 5 },
		{ 3, 5 }
	};
	const static ulong2 bia[] = {
		{ 0, 0 },	{ 1, 0 },	{ 2, 0 },
						{ 2, 1 },
						{ 2, 2 },	{ 3, 2 },	{ 4, 2 },

						{ 2, 0 },	{ 3, 0 },	{ 4, 0 },
						{ 2, 1 },
		{ 0, 2 },	{ 1, 2 },	{ 2, 2 },

		{ 0, 0 },
		{ 0, 1 },
		{ 0, 2 },	{ 1, 2 },	{ 2, 2 },
						{ 2, 3 },
						{ 2, 3 },

						{ 2, 0 },
						{ 2, 1 },
		{ 0, 2 },	{ 1, 2 },	{ 2, 2 },
		{ 0, 3 },
		{ 0, 4 }
	};
	const static size_t n_lim = sizeof( lim ) / sizeof( *lim );
	const static size_t n_bia = sizeof( bia ) / sizeof( *bia ) / n_lim;

	ulong id = get_global_id( 0 );
	ulong stride = get_global_size( 0 );
	char res = 1;
	char tmp = 0;

	for ( ulong i = id; i < *( size + dim ) && res; i += stride ){
		for ( ulong x = 0; x < dim; x++ ){
			for ( ulong y = x + 1; y < dim; y++ ){
				for ( ulong ii = 0; ii < n_lim; ii++ ){
					if (
						*( dims + x ) - ( ( i % *( size + x + 1 ) ) / ( *( size + x ) ) ) >= ( *( lim + ii ) ).x &&
						*( dims + y ) - ( ( i % *( size + y + 1 ) ) / ( *( size + y ) ) ) >= ( *( lim + ii ) ).y
					){
						tmp = 0;
						for ( ulong iii = 0; iii < n_bia; iii++ ){
							tmp =
								tmp ||
								find( pts,
								i +
								( *( bia + n_bia * ii + iii ) ).x * *( size + x ) +
								( *( bia + n_bia * ii + iii ) ).y * *( size + y ),
								len, 0 );
						}
						res = res && tmp;
					}
				}
			}
		}
	}
/*
	for ( ulong i = id; i < *( size + dim ) && res; i += stride ){
		for ( ulong ii = 0; ii < n_lim; ii++ ){
			if (
				*( dims + 0 ) - ( ( i % *( size + 1 ) ) / ( *( size + 0 ) ) ) >= ( *( lim + ii ) ).x &&
				*( dims + 1 ) - ( ( i % *( size + 2 ) ) / ( *( size + 1 ) ) ) >= ( *( lim + ii ) ).y
			){
				tmp = 0;
				for ( ulong iii = 0; iii < n_bia; iii++ ){
					tmp =
						tmp ||
						find( pts,
						i +
						( *( bia + n_bia * ii + iii ) ).x * *( size + 0 ) +
						( *( bia + n_bia * ii + iii ) ).y * *( size + 1 ),
						len, 0 );
				}
				res = res && tmp;
			}
		}
	}
*/
	*( ret + id ) = res;
}

/*
Binary Search
*/
/*
int find( __global const ulong *set, ulong target, ulong r, ulong l ){
	ulong m;
	r--;
	while ( r >= l ){
		m = ( ( r - l ) >> 1 ) + l;
		if ( *( set + m ) == target ){
			return 1;
		}else if ( *( set + m ) < target ){
			l = m + 1;
		}else{
			r = m - 1;
		}
	}
	return 0;
}
*/

/*
Linear Search
*/
int find( __global const ulong *set, ulong target, ulong r, ulong l ){
	for ( ; l < r; l++ ){
		if ( *( set + l ) == target ){
			return 1;
		}
	}
	return 0;
}
