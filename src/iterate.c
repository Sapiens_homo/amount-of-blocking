
#include "../include/func.h"
#include "../include/gpu.h"

#include <mpi.h>
#include <unistd.h>
#include <stdio.h>

#include <time.h>

#define ___PASTE( a, b ) a##b
#define __PASTE(a, b) ___PASTE(a, b)
#define _UNIQUE_ID(a) __PASTE( __PASTE( _UNIQUE_ID_, a ), __COUNTER__ )
#define __min( a, b, t1, t2 ) __extension__({	\
		register __typeof__(a) t1 = a;	\
		register __typeof__(b) t2 = b;	\
		( t1 < t2 )?(t1):(t2);		\
		} )
#define min( a, b ) __min( a, b, _UNIQUE_ID(a_), _UNIQUE_ID(b_) )

int iterate( size_t dim, size_t *restrict size, size_t n, struct __CL_MEM__ g_mem, struct CL_RES *cl_res ){
#define EXIT( r ) if ( r != CL_SUCCESS ){ CLERROR( r ); goto ERR; }
	register cl_int *p_ret = NULL;
	register int ret;

	register size_t *pts = NULL;

	register size_t i;

	int cl_err;
	cl_event event;

	if ( !n ){ return 0; }
	if ( !( n - *( size + dim ) ) ){ return 1; }

	cl_err = clSetKernelArg( ( *cl_res ).kernel, 0, sizeof( cl_mem ), &g_mem.ret );
	EXIT( cl_err );
	cl_err = clSetKernelArg( ( *cl_res ).kernel, 1, sizeof( g_mem.dim ), &g_mem.dim);
	EXIT( cl_err );
	cl_err = clSetKernelArg( ( *cl_res ).kernel, 2, sizeof( cl_mem ), &g_mem.dims );
	EXIT( cl_err );
	cl_err = clSetKernelArg( ( *cl_res ).kernel, 3, sizeof( cl_mem ), &g_mem.size );
	EXIT( cl_err );
	cl_err = clSetKernelArg( ( *cl_res ).kernel, 4, sizeof( n ), &n );
	EXIT( cl_err );
	cl_err = clSetKernelArg( ( *cl_res ).kernel, 5, sizeof( cl_mem ), &g_mem.pts );
	EXIT( cl_err );
	cl_err = clSetKernelArg( ( *cl_res ).kernel, 6, sizeof( g_mem.len ), &g_mem.len );
	EXIT( cl_err );
	cl_err = clSetKernelArg( ( *cl_res ).kernel, 7, sizeof( cl_mem ), &g_mem.mem );
	EXIT( cl_err );

	cl_err = clEnqueueNDRangeKernel( ( *cl_res ).queue, ( *cl_res ).kernel, 1, NULL, ( *cl_res ).size, NULL, 0, NULL, &event );
	EXIT( cl_err );

	p_ret = malloc( sizeof( *p_ret ) );
	pts = malloc( sizeof( *pts ) * n );

	cl_err = clFinish( ( *cl_res ).queue );
	CLERROR( cl_err );
	if ( cl_err < 0 ){
		cl_err = clGetEventInfo( event, CL_EVENT_COMMAND_EXECUTION_STATUS, sizeof( *p_ret ), p_ret, NULL );
		EXIT( cl_err );
	}

	cl_err = clEnqueueReadBuffer( ( *cl_res ).queue, g_mem.ret, CL_TRUE, 0, sizeof( *p_ret ), p_ret, 0, NULL, NULL );
	EXIT( cl_err );

#ifdef DBG
	fprintf( stderr, "[%lu] [n=%lu] ret: %d\n", time( NULL ), n, *p_ret );
#endif
	cl_err = clEnqueueReadBuffer( ( *cl_res ).queue, g_mem.pts, CL_TRUE, 0, sizeof( *pts ) * n, pts, 0, NULL, NULL );
	EXIT( cl_err );

	printf( "[%lu] [n=%lu] Points:", time( NULL ), n );
	if ( ( ret = !!*p_ret ) ){
		for ( i = 0; i < n; i++ ){
			printf( " %lu", *( pts + i ) );
		}
	}else{
		printf( " none" );
	}
	putchar( 0x0A );

	free( p_ret );
	free( pts );
	return ret;

ERR:
	printf( "[%lu] [n=%lu] Points: ERROR\n", time( NULL ), n );
	free( p_ret );
	free( pts );
	return cl_err;
}
