CC = mpicc
CFLAGS = -lpthread -mtune=native -march=native -Wall -lm -lOpenCL -D CL_TARGET_OPENCL_VERSION=300 -D MPI
CFLAGS_DBG = $(CFLAGS) -g -D DBG
C_FILES = $(shell find src -name "*.c")
OBJ = $(patsubst %.c, %.o, $(C_FILES) )
OBJ_DBG = $(patsubst %.c, %.o.dbg, $(C_FILES) )
OBJ_FILE = $(shell find . -name "*.o*")
OUTPUT = release

.PHONY: all debug clean
all: $(OUTPUT)

debug: $(OUTPUT).dbg

$(OUTPUT): $(OBJ)
	@echo Linking $@
	@$(CC) $^ $(CFLAGS) -o $@

$(OUTPUT).dbg: $(OBJ_DBG)
	@echo Linking $@
	@$(CC) $^ $(CFLAGS_DBG) -o $@

%.o: %.c
	@echo Compiling $@
	@$(CC) $^ $(CFLAGS) -c -o $@

%.o.dbg: %.c
	@echo Compiling $@
	@$(CC) $^ $(CFLAGS_DBG) -c -o $@
clean:
	@echo Cleaning
	-@rm $(OUTPUT)*
	-@rm $(OBJ_FILE)
