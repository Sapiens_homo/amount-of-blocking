
#include "../include/func.h"
#include "../include/gpu.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <getopt.h>

#include <mpi.h>
#include <math.h>

#include <limits.h>

#ifdef DBG
#include <time.h>
#endif

#define min( a, b ) __extension__({						\
	register __typeof__(a) _a = (a); register __typeof__(b) _b = (b);	\
	( (_a) < (_b) )?(_a):(_b);						\
})

signed main( int argc, char **argv ){
#define CERROR( n, r ) { fprintf( stderr, "[rank %u] ", n ); CLERROR( r ); }
#define EXIT( n, r, T ) if ( r != CL_SUCCESS ){ CERROR( n, r ); MPI_Finalize(); goto T; }

	size_t dim;
	register size_t *dims = NULL;
	register size_t *size = NULL;

	register size_t *lim = NULL;
	register size_t base = 0;
	register size_t n;
	register size_t stride;
	register int *res = NULL;

	register struct CL_RES *cl_res = NULL;
	register struct __CL_MEM__ g_mem = {};

	register int opt;

	int cl_err;

#ifdef MPI
	signed mpi_process;
	signed mpi_rank;
	signed mpi_local_rank;
	register size_t tmp;

	MPI_Init( &argc, &argv );
	MPI_Comm_size( MPI_COMM_WORLD, &mpi_process );
	MPI_Comm_rank( MPI_COMM_WORLD, &mpi_rank );
	{
		MPI_Comm local_comm;
		MPI_Comm_split_type( MPI_COMM_WORLD, MPI_COMM_TYPE_SHARED, mpi_rank, MPI_INFO_NULL, &local_comm );
		MPI_Comm_rank( local_comm, &mpi_local_rank );
		MPI_Comm_free( &local_comm );
	}
#else
#define mpi_rank 0
#define mpi_local_rank 0
#endif

	dim = 0;
	while ( ( opt = getopt( argc, argv, "d" ) ) != -1 ){
		switch ( opt ){
			case 'd':
				dim = atol( *( argv + optind ) );
				if ( dim > 256 ){
					fprintf( stderr, "%ld dimesions: too many, max: 256\n", dim );
					exit( EXIT_FAILURE );
				}
MLC0:
				if ( !( dims = malloc( sizeof( *dims ) * dim ) ) ){
					goto MLC0;
				}
				if ( optind >= argc || (unsigned)( argc - optind ) < dim ){
					goto READ;
				}
				optind++;
				for ( register unsigned i = 0; i < dim; i++ ){
					*( dims + i ) = atol( *( argv + i + optind ) );
				}
				break;
			case '?':
				goto GET;
			default:
				break;
		}
	}

GET:
	if ( !dim ){
#ifdef MPI
		if ( !mpi_rank ){
#endif
		printf( "How many dimensions: " );
#ifdef MPI
		}
#endif
		scanf( "%lu", &dim );
		if ( dim > 256 ){
			fprintf( stderr, "%ld dimesions: too many, max: 256\n", dim );
			exit( EXIT_FAILURE );
		}
MLC1:
		if ( !( dims = malloc( sizeof( *dims ) * dim ) ) ){
			goto MLC1;
		}
READ:
#ifdef MPI
		if ( !mpi_rank ){
#endif
		printf( "How long in each dimension: " );
#ifdef MPI
		}
#endif
		for ( register size_t i = 0; i < dim; i++ ){
			scanf( "%lu", dims + i );
		}
	}

#ifdef DBG
#ifdef MPI
	if ( !mpi_rank ){
#endif
	fprintf( stderr, "[rank %d] [ %lu ] [Debugging info]\nDimensions : %lu\nLength of sides: { ", mpi_rank, time( NULL ), dim );
	for ( register size_t i = 0; i < dim; i++ ){
		fprintf( stderr, "%lu ", *( dims + i ) );
	}
	fputc( '}', stderr );
	fputc( 10, stderr );
#ifdef MPI
	}
#endif
#endif
MLC2:
	if ( !( size = malloc( sizeof( *size ) * ( dim + 1 ) ) ) ){
		goto MLC2;
	}
	( *size ) = 1;
	for ( register size_t i = 0; i < dim; i++ ){
		*( size + i + 1 ) = *( size + i ) * *( dims + i );
	}

MLC3:
	if ( !( cl_res = malloc( sizeof( *cl_res ) ) ) ){
		goto MLC3;
	}
	memset( cl_res, 0, sizeof( *cl_res ) );

MLC4:
	if ( !( lim = malloc( sizeof( *lim ) << 1 ) ) ){
		goto MLC4;
	}
	
MLC5:
	if ( !( res = malloc( sizeof( *res ) ) ) ){
		goto MLC5;
	}

	if ( ( cl_err = cl_init( cl_res, 0, mpi_local_rank ) )!= CL_SUCCESS ){
		EXIT( mpi_rank, cl_err, END );
	}

#ifdef DBG
#ifdef MPI
	if ( !mpi_rank ){
#endif
	fprintf( stderr, "[rank %d] [ %lu ] [Debugging info]\nsize: { ", mpi_rank, time( NULL ) );
	for ( register size_t i = 0; i <= dim; i++ ){
		fprintf( stderr, "%lu ", *( size + i ) );
	}
	fputc( '}', stderr );
	fputc( 10, stderr );
#ifdef MPI
	}
#endif
#endif

#ifdef MAX_WORKER
	*( ( *cl_res ).size + 0 ) = min( *( ( *cl_res ).size + 0 ), MAX_WORKER );
#endif
	*( ( *cl_res ).size + 3 ) = min( *( ( *cl_res ).size + 0 ), *( ( *cl_res ).size + 3 ) );
	g_mem.len = min( *( ( *cl_res ).size + 0 ) * sizeof( *( size + dim ) ) * *( size + dim ), ( *cl_res ).mem );
	g_mem.dim = dim;
	g_mem.ret = clCreateBuffer(
			( *cl_res ).context,
			CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY,
			sizeof( cl_int ),
			NULL,
			&cl_err
	);
	EXIT( mpi_rank, cl_err, END );
	g_mem.dims = clCreateBuffer(
			( *cl_res ).context,
			CL_MEM_READ_ONLY | CL_MEM_HOST_NO_ACCESS | CL_MEM_USE_HOST_PTR,
			sizeof( *dims ) * dim,
			dims,
			&cl_err
	);
	EXIT( mpi_rank, cl_err, END );
	g_mem.size = clCreateBuffer(
			( *cl_res ).context,
			CL_MEM_READ_ONLY | CL_MEM_HOST_NO_ACCESS | CL_MEM_USE_HOST_PTR,
			sizeof( *size ) * ( dim + 1 ),
			size,
			&cl_err
	);
	EXIT( mpi_rank, cl_err, END );
	g_mem.pts = clCreateBuffer(
			( *cl_res ).context,
			CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY,
			sizeof( *( size + dim ) ) * *( size + dim ),
			NULL,
			&cl_err
	);
	EXIT( mpi_rank, cl_err, END );
	g_mem.mem = clCreateBuffer(
			( *cl_res ).context,
			CL_MEM_READ_WRITE | CL_MEM_HOST_NO_ACCESS,
			g_mem.len,
			NULL,
			&cl_err
	);
	EXIT( mpi_rank, cl_err, END );

	*( lim + 0 )= 0;
	*( lim + 1 ) = *( size + dim );
	(void)base;

#ifdef DBG
	fprintf( stderr, "[rank %d] Global: %lu, Local: %lu, Mem: %lu (%lu K)\n", mpi_rank, *( ( *cl_res ).size + 0 ), *( ( *cl_res ).size + 3 ), g_mem.len, g_mem.len >> 10 );
#endif

	while ( *( lim + 1 ) - *( lim + 0 ) - 1 ){
#ifdef MPI
		 stride = ( *( lim + 1 ) - *( lim + 0 ) ) / ( mpi_process + 1 );
#else
		 stride = ( *( lim + 1 ) - *( lim + 0 ) ) >> 1;
#endif
		if ( !stride ){
			stride = 1;
		}
#ifdef MPI
		n = *lim + ( stride * ( mpi_rank + 1 ) );
#else
		n = *lim + stride;
#endif
		if ( n >= *( lim + 1 ) ){
#ifdef MPI
			*res = 1;
			goto SYNC;
#else
			break;
#endif
		}

		*res = iterate( dim, size, n, g_mem, cl_res );
		if ( *res < 0 ){
			CLERROR( *res );
		}

#ifdef MPI

SYNC:
		MPI_Barrier( MPI_COMM_WORLD );
		if ( mpi_rank ){
			MPI_Send( res, 1, MPI_INT, 0, mpi_rank, MPI_COMM_WORLD );
			MPI_Recv( lim, 2, MPI_LONG, 0, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
		}else{
#endif
#ifdef DBG
#ifdef MPI
			if ( !mpi_rank ){
#endif
			fprintf( stderr, "lim: ( %lu, %lu ), stride: %lu\n", *lim, *( lim + 1 ), stride );
#ifdef MPI
			}
#endif
#endif
			base = *lim;
			if ( *res ){
				*( lim + 1 ) = n;
			}else{
				*( lim + 0 ) = n;
			}
#ifdef MPI
			for ( register signed i = 1; i < mpi_process; i++ ){
				MPI_Recv( res, 1, MPI_INT, i, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
				tmp = stride * ( i + 1 ) + base;
				if ( *res ){
					*( lim + 1 ) = ( *( lim + 1 ) > tmp ) ? tmp : *( lim + 1 );
				}else{
					*( lim + 0 ) = ( *( lim + 0 ) < tmp ) ? tmp : *( lim + 0 );
				}
			}
			for ( register signed i = 1; i < mpi_process; i++ ){
				MPI_Send( lim, 2, MPI_LONG, i, i, MPI_COMM_WORLD );
			}
		}
		MPI_Barrier( MPI_COMM_WORLD );

#endif
	}

#ifdef MPI
	MPI_Finalize();
	if ( mpi_rank ){
		goto END;
	}
#endif

	printf( "Min amount to block: %lu\n", *( lim + 1 ) );

END:
	free( size );
	free( dims );
	free( lim );
	free( res );

	cl_err = clReleaseMemObject( g_mem.dims );
	CLERROR( cl_err );
	cl_err = clReleaseMemObject( g_mem.size );
	CLERROR( cl_err );
	cl_err = clReleaseMemObject( g_mem.pts );
	CLERROR( cl_err );
	cl_err = clReleaseMemObject( g_mem.ret );
	CLERROR( cl_err );
	cl_err = clReleaseMemObject( g_mem.mem );
	CLERROR( cl_err );

	cl_err = clReleaseContext( ( *cl_res ).context );
	CLERROR( cl_err );
	cl_err = clReleaseKernel( ( *cl_res ).kernel );
	CLERROR( cl_err );
	cl_err = clReleaseCommandQueue( ( *cl_res ).queue );
	CLERROR( cl_err );
	free( cl_res );
}
