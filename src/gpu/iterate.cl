
__kernel void iterate(
	__global int *STATUS,
	const ulong dim,
	__global const ulong *dims,
	__global const ulong *size,
	const ulong n,
	__global ulong *PTS,
	const ulong len,
	__global ulong *p_pts
){
	const static ulong2 lim[] = {
		{ 5, 3 },
		{ 5, 3 },
		{ 3, 5 },
		{ 3, 5 }
	};
	const static ulong2 bia[] = {
		{ 0, 0 },	{ 1, 0 },	{ 2, 0 },
						{ 2, 1 },
						{ 2, 2 },	{ 3, 2 },	{ 4, 2 },

						{ 2, 0 },	{ 3, 0 },	{ 4, 0 },
						{ 2, 1 },
		{ 0, 2 },	{ 1, 2 },	{ 2, 2 },

		{ 0, 0 },
		{ 0, 1 },
		{ 0, 2 },	{ 1, 2 },	{ 2, 2 },
						{ 2, 3 },
						{ 2, 3 },

						{ 2, 0 },
						{ 2, 1 },
		{ 0, 2 },	{ 1, 2 },	{ 2, 2 },
		{ 0, 3 },
		{ 0, 4 }
	};
	const static size_t n_lim = sizeof( lim ) / sizeof( *lim );
	const static size_t n_bia = sizeof( bia ) / sizeof( *bia ) / n_lim;

	ulong gid = get_global_id( 0 );
	ulong gsize = get_global_size( 0 );

	int ret;
	int tmp;

	ulong index;
	ulong rem;
	__global ulong *pts = p_pts + n * gid;

	ulong l, r, m, t;
	int _;

	if ( !gid ){ atomic_store( ( volatile __global atomic_int * )STATUS, 0 ); }

	if ( gid * ( n + 1 ) * sizeof( *pts ) > len ){ goto END; }

	if ( gsize * n * sizeof( *pts ) > len ){
		gsize = len / ( n * sizeof( *pts ) ) * ( n * sizeof( *pts ) );
	}

	ret = 0;

	/* init */
	for ( index = 0; index < n; index++ ){
		*( pts + index ) = index;
	}
	rem = gid;
	index = n - 1;
	while ( rem ){
		while ( index < n ){
			if ( *( pts + index ) >= *( size + dim ) ){
				if ( !index ){
					break;
				}
				index--;
				*( pts + index ) += 1;
				continue;
			}
			if ( index == n - 1 ){
				if ( *( size + dim ) > *( pts + index ) + rem ){
					*( pts + index ) += rem;
					rem = 0;
					break;
				}
				rem -= *( size + dim ) - *( pts + index );
				*( pts + index ) = *( size + dim );
				continue;
			}else{
				*( pts + index + 1 ) = *( pts + index ) + 1;
			}
			index++;
		}
		if ( rem ){
			goto END;
		}
	}

	while ( *pts < *( size + dim ) ){
		if ( atomic_load( ( volatile __global atomic_int * )STATUS ) ){
			break;
		}

		/* check */
		ret = 1;

		for ( ulong i = 0; i < *( size + dim ) && ret; i ++ ){
			for ( ulong x = 0; x < dim; x++ ){
				for ( ulong y = x + 1; y < dim; y++ ){
					for ( ulong ii = 0; ii < n_lim; ii++ ){
						if (
							*( dims + x ) - ( ( i % *( size + x + 1 ) ) / ( *( size + x ) ) ) >= ( *( lim + ii ) ).x &&
							*( dims + y ) - ( ( i % *( size + y + 1 ) ) / ( *( size + y ) ) ) >= ( *( lim + ii ) ).y
						){
							tmp = 0;
							for ( ulong iii = 0; iii < n_bia; iii++ ){
								_ = 0;
								l = 0;
								r = n;
								while ( r-- ){
									if (
										( _ =
											i + ( *( bia + n_bia * ii + iii ) ).x * *( size + x ) + ( *( bia + n_bia * ii + iii ) ).y * *( size + y )
											== *( pts + r )
										)
									){
										break;
									}
								}
								/*
								r--;
								while ( l <= r ){
									m = ( ( r - l ) >> 1 ) + l;
									t = i + ( *( bia + n_bia * ii + iii ) ).x * *( size + x ) + ( *( bia + n_bia * ii + iii ) ).y * *( size + y );
									if ( *( pts + m ) == t ){
										_ = 1;
										break;
									}else if ( *( pts + m ) < t ){
										l = m + 1;
									}else{
										r = m - 1;
									}
								}
								*/
								tmp = tmp || _;
							}
							ret = ret && tmp;
						}
					}
				}
			}
		}

		if ( !atomic_cmpxchg( STATUS, 0, ret ) && ret ){
			for ( index = 0; index < n; index++ ){
				*( PTS + index ) = *( pts + index );
			}
		}

		if ( ret ){ break; }

		/* iterating */
		rem = gsize;
		index = n - 1;
		while ( rem && index < n ){
			if ( *( pts + index ) >= *( size + dim ) ){
				if ( !index ){
					break;
				}
				index--;
				*( pts + index ) += 1;
				continue;
			}
			if ( index == n - 1 ){
				if ( *( size + dim ) > *( pts + index ) + rem ){
					*( pts + index ) += rem;
					rem = 0;
					break;
				}
				rem -= *( size + dim ) - *( pts + index );
				*( pts + index ) = *( size + dim );
				continue;
			}else{
				*( pts + index + 1 ) = *( pts + index ) + 1;
			}
			index++;
		}
		if ( rem ){
			break;
		}
	}

END:
	return;
}

int find( __global const ulong *arr, ulong target, ulong r, ulong l ){
	ulong m;
	r--;
	while ( r >= l ){
		m = ( ( r - l ) >> 1 ) + l;
		if ( *( arr + m ) == target ){
			return 1;
		}else if ( *( arr + m ) < target ){
			l = m + 1;
		}else{
			r = m - 1;
		}
	}
	return 0;
}
