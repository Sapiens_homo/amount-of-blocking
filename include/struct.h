
#ifndef __STRUCT_H__

#define __STRUCT_H__

#include <stddef.h>

struct _NUMBER_ {
	const size_t BITS;
	const size_t len;
	char *val;
};

struct _NUMBER_ num( size_t val, size_t wid );
void add ( struct _NUMBER_ dst, struct _NUMBER_ val1, struct _NUMBER_ val2 );
int lt( struct _NUMBER_, struct _NUMBER_ );
typedef struct _NUMBER_ num_t;

#endif
