#ifndef __LIB_H__
#define __LIB_H__

#include <stdlib.h>

#include "gpu.h"

struct __CL_MEM__ {
	cl_mem ret;
	size_t dim;
	cl_mem dims;
	cl_mem size;
	size_t len;
	cl_mem pts;
	cl_mem mem;
};

int iterate( size_t dim, size_t *restrict size, size_t n, struct __CL_MEM__, struct CL_RES * );

size_t get_memory_size( void );

size_t print_block( size_t len, size_t *pt, size_t dim, size_t *size );

#endif
