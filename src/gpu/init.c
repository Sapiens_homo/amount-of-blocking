
#include "../../include/gpu.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/syscall.h>
#include <unistd.h>
#include <fcntl.h>

#include <math.h>

/*
#define CL_SOURCE_CODE "src/gpu/check.cl"
#define KERNEL_NAME "check"
*/
/* void check ( int *ret, size_t dim, size_t *size, size_t len, size_t *pts ) */

#define CL_SOURCE_CODE "src/gpu/iterate.cl"
#define KERNEL_NAME "iterate"
/* void iterate ( int *status, size_t dim, size_t *dims, size_t *size, size_t n, size_t *pts, size_t len, __local size_t *tmp ) */

int cl_init( struct CL_RES *restrict res, size_t platform, size_t gpu ){
#define EXIT( e ) if ( e != CL_SUCCESS ){ CLERROR( e ); goto ERR; }
	int cl_err = 0;

	char *content = NULL;
	size_t content_len;
	register signed fd;
	cl_uint n_platforms;
	register cl_platform_id *platforms;
	cl_program program;
	cl_uint n_devices;
	register cl_device_id *devices = NULL;

#ifdef DBG
	char *vender = NULL;
	size_t vender_len = 0;
#endif

	if ( !res ){
		fprintf( stderr, "cl_init: res cannot be NULL\n" );
		return -1;
	}
	memset( res, 0, sizeof( *res ) );
	cl_err = clGetPlatformIDs( 0, NULL, &n_platforms );
	platforms = malloc( sizeof( platforms ) * n_platforms );
	platform = platform % n_platforms;

	cl_err = clGetPlatformIDs( n_platforms, platforms, NULL );
	EXIT( cl_err );

	cl_err = clGetDeviceIDs( *( platforms + platform ), CL_DEVICE_TYPE_GPU, 0, NULL, &n_devices );
	EXIT( cl_err );
	devices = malloc( sizeof( devices ) * n_devices );
	memset( devices, 0, sizeof( devices ) * n_devices );
	cl_err = clGetDeviceIDs( *( platforms + platform ), CL_DEVICE_TYPE_GPU, n_devices, devices, NULL );
	EXIT( cl_err );

	( *res ).device = *( devices + ( gpu % n_devices ) );

#ifdef DBG
	vender = malloc( 1024 );
	for ( register unsigned i = 0; i < n_devices; i++ ){
		CLERROR( clGetDeviceInfo( *( devices + i ), CL_DEVICE_VENDOR, 1024, vender, &vender_len ) );
		fprintf( stderr, "Device #%u vender: %s\n", i, vender );
	}
	free( vender );
#endif

	( *res ).context = clCreateContext( NULL, 1, &( *res ).device, NULL, NULL, &cl_err );
	EXIT( cl_err );

	fd = syscall( SYS_openat, AT_FDCWD, CL_SOURCE_CODE, O_RDONLY );
	if ( fd <= 0 ){
		fprintf( stderr, "Cannot Open File: %s\n", CL_SOURCE_CODE );
		goto ERR;
	}
	content_len = syscall( SYS_lseek, fd, 0, SEEK_END );
	if ( content_len == ( __typeof__( content_len ) )( off_t )( -1 ) ){
		fprintf( stderr, "Content Error\n" );
		goto ERR;
	}
	content = malloc( content_len + 1 );
	syscall( SYS_lseek, fd, 0, SEEK_SET );
	syscall( SYS_read, fd, content, content_len );
	syscall( SYS_close, fd );
	*( content + content_len ) = 0;
	program = clCreateProgramWithSource( ( *res ).context, 1, ( void * )&content, NULL, &cl_err );
	EXIT( cl_err );
	free( content );
	content = NULL;

	cl_err = clBuildProgram( program, 1, &( *res ).device, "-cl-std=CL3.0", NULL, NULL );
	if ( cl_err < 0 ){
		clGetProgramBuildInfo( program, ( *res ).device, CL_PROGRAM_BUILD_LOG, 0, NULL, &content_len );
		content = malloc( content_len + 1 );
		clGetProgramBuildInfo( program, ( *res ).device, CL_PROGRAM_BUILD_LOG, content_len + 1, content, NULL );
		*( content + content_len ) = 10;
		syscall( SYS_write, 2, content, content_len + 1 );
		free( content );
		content = NULL;
	}
	EXIT( cl_err );

	( *res ).kernel = clCreateKernel( program, KERNEL_NAME, &cl_err );
	EXIT( cl_err );

	cl_err = clGetKernelWorkGroupInfo( ( *res ).kernel, ( *res ).device, CL_KERNEL_GLOBAL_WORK_SIZE, sizeof( *( *res ).size ) * 3, ( *res ).size, NULL );
	cl_err = clGetDeviceInfo( ( *res ).device, CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof( *( *res ).size ) * 3, ( *res ).size, NULL );
	EXIT( cl_err );
	cl_err = clGetKernelWorkGroupInfo( ( *res ).kernel, ( *res ).device, CL_KERNEL_WORK_GROUP_SIZE, sizeof( *( *res ).size ), ( *res ).size + 3, NULL );
	EXIT( cl_err );
	cl_err = clGetKernelWorkGroupInfo( ( *res ).kernel, ( *res ).device, CL_KERNEL_LOCAL_MEM_SIZE, sizeof( ( *res ).local ), &( *res ).local, NULL );
	EXIT( cl_err );
	cl_err = clGetKernelWorkGroupInfo( ( *res ).kernel, ( *res ).device, CL_KERNEL_PRIVATE_MEM_SIZE, sizeof( ( *res ).priv ), &( *res ).priv, NULL );
	EXIT( cl_err );
	cl_err = clGetDeviceInfo( ( *res ).device, CL_DEVICE_MAX_MEM_ALLOC_SIZE, sizeof( ( *res ).mem ), &( *res ).mem, NULL );
	EXIT( cl_err );

#ifdef DBG
	fprintf( stderr, "Device #%lu: Global Size: %lu, Local Size: %lu\nMem: %lu (%lu M), %lu (%lu M), %lu (%lu M)\n", gpu % n_devices, *( ( *res ).size + 0 ), *( ( *res ).size + 3 ), ( *res ).mem, ( *res ).mem >> 20, ( *res ).local, ( *res ).local >> 20,  ( *res ).priv, ( *res ).priv >> 20 );
#endif
	( *res ).queue = clCreateCommandQueueWithProperties( ( *res ).context, ( *res ).device, NULL, &cl_err );
	EXIT( cl_err );

RET:
	free( platforms );
	return cl_err;
ERR:
	free( devices );
	clReleaseCommandQueue( ( *res ).queue );
	clReleaseContext( ( *res ).context );
	clReleaseKernel( ( *res ).kernel );
	free( content );
	memset( res, 0, sizeof( *res ) );

	goto RET;
}
