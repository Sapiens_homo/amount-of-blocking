
#include <stdio.h>
#include <stdlib.h>

#include <getopt.h>

#include <unistd.h>
#include <sys/syscall.h>

#define BUF_LEN (512)

signed main( int argc, char **argv ){

	register int opt;

	size_t dim;
	size_t n;
	register size_t *dims = NULL;
	register size_t size;
	register size_t *pts = NULL;
	register size_t i;
	register size_t ii;

	register char *buf = NULL;
	register size_t len;

	while ( ( opt = getopt( argc, argv, "dn" ) ) != -1 ){
		switch ( opt ){
			case 'd':
				dim = atol( *( argv + optind ) );
				if ( dim > 256 ){
					fprintf( stderr, "%ld dimesions: too many, max: 256\n", dim );
					exit( EXIT_FAILURE );
				}
MLC0:
				if ( !( dims = malloc( sizeof( *dims ) * dim ) ) ){
					goto MLC0;
				}
				if ( optind >= argc || (unsigned)( argc - optind ) < dim ){
					goto READ;
				}
				optind++;
				for ( i = 0; i < dim; i++ ){
					*( dims + i ) = atol( *( argv + i + optind ) );
				}
				break;
			case 'n':
				n = atol( *( argv + optind ) );
				if ( optind >= argc || (unsigned)( argc - optind ) < n ){
					goto READ_N;
				}
MLC4:
				if ( !( pts = malloc( sizeof( *pts ) * n ) ) ){
					goto MLC4;
				}
				optind++;
				for ( i = 0; i < n; i++ ){
					*( pts + i ) = atol( *( argv + i + optind ) );
				}
				break;
			case '?':
				goto GET;
			default:
				break;
		}
	}

GET:
	if ( !dim ){
		printf( "How many dimensions: " );
		scanf( "%lu", &dim );
MLC1:
		if ( !( dims = malloc( sizeof( *dims ) * dim ) ) ){
			goto MLC1;
		}
READ:
		printf( "How long in each dimension: " );
		for ( i = 0; i < dim; i++ ){
			scanf( "%lu", dims + i );
		}

		printf( "How many points: " );
		scanf( "%lu", &n );
MLC2:
		if ( !( pts = malloc( sizeof( *pts ) * n ) ) ){
			goto MLC2;
		}
READ_N:
		for ( i = 0; i < n; i++ ){
			scanf( "%lu", pts + i );
		}
	}

	size = 1;
	for ( i = 0; i < dim; i++ ){
		size *= *( dims + i );
	}

	i = 0;
	ii = 0;
	buf = malloc( BUF_LEN + 2 );
	len = 0;

	for ( i = 0; i < size; i++ ){
		( i % *dims ) || ( *( buf + len++ ) = 10 );
		*( buf + len++ ) = ( i == *( pts + ii ) ) ^ 48;
		ii += i == *( pts + ii );
		( len - BUF_LEN ) || ( syscall( SYS_write, 1, buf, len ), len = 0 );
	}
	len && syscall( SYS_write, 1, buf, len );
	*buf = 10;
	syscall( SYS_write, 1, buf, 1 );

	free( dims );
	free( pts );
	free( buf );
}
