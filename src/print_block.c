
#include "../include/func.h"

#include <unistd.h>
#include <sys/syscall.h>
#include <stdio.h>

size_t print_block( size_t len, size_t *pt, size_t dim, size_t *size ){
	register size_t ret = 0;
	register size_t i;
	register size_t j;

	ret += printf( "Blocks: {\n" );
	for ( i = 0; i < len; i++ ){
		j = dim;
		putchar( '[' );
		putchar( 32 );
		while ( j-- ){
			ret += printf( "%04ld ", ( ( *( pt + i ) % *( size + j + 1 ) ) % *( size + j ) ) );
		}
		putchar( ']' );
		putchar( 10 );
		ret += 4;
	}
	ret += printf( "}\n" );

	return ret;
}
